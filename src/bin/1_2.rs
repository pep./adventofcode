// Copyright (C) 2021 "Maxime “pep” Buquet <pep@bouah.net>"
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#![feature(stdin_forwarders)]

use aoc2021::Error;
use std::io::stdin;
use std::iter::Iterator;

fn get_sliding_increased_measurements<I>(lines: I) -> Result<usize, Error>
    where I: Iterator<Item = usize>
{
    let mut measurements: Vec<Vec<usize>> = Vec::new();

    let mut res: usize = 0;
    let mut last_i = None;

    for (i, val) in lines.enumerate() {
        measurements.push(vec![val]);

        if i > 0 {
            measurements[i - 1].push(val);
        }

        if i > 1 {
            measurements[i - 2].push(val);
        }
    }

    for values in measurements {
        let sum: usize = values.iter().sum::<usize>();
        if let Some(prev) = last_i {
            if prev < sum {
                res = res + 1;
            }
        }

        last_i = Some(sum);
    }

    Ok(res)
}

fn main() -> Result<(), Error> {
    let lines = stdin().lines();

    let mut int_lines = vec![];
    for line in lines {
        int_lines.push(line?.parse::<usize>()?);
    }

    let res = get_sliding_increased_measurements(int_lines.into_iter())?;
    println!("There are {} measurements that are larger than the previous measurement.", res);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn triples_increased_measurements() {
        let lines: Vec<&str> = vec![
            "199",
            "200",
            "208",
            "210",
            "200",
            "207",
            "240",
            "269",
            "260",
            "263",
        ];

        let mut int_lines = vec![];
        for line in lines {
            int_lines.push(line.parse::<usize>().unwrap());
        }

        assert_eq!(get_sliding_increased_measurements(int_lines.into_iter()).unwrap(), 5);
    }
}
