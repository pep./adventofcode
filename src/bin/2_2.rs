// Copyright (C) 2021 "Maxime “pep” Buquet <pep@bouah.net>"
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#![feature(stdin_forwarders)]

use aoc2021::Error;
use std::io::stdin;
use std::iter::Iterator;
use std::char;

#[derive(Debug, Eq, PartialEq)]
enum Movement {
    Forward(i64),
    Up(i64),
    Down(i64),
}

fn parse_line(input: &str) -> Result<Movement, Error> {
    let split: Vec<&str> = input.split(char::is_whitespace).collect();
    let val = split[1].parse::<i64>()?;

    Ok(match split[0] {
        "forward" => Movement::Forward(val),
        "up" => Movement::Up(val),
        "down" => Movement::Down(val),
        _ => unreachable!(),
    })
}

fn get_total<I>(lines: I) -> Result<i64, Error>
    where I: Iterator<Item = Movement>
{
    let mut position = 0;
    let mut depth = 0;
    let mut aim = 0;

    for line in lines {
        match line {
            Movement::Forward(i) => {
                position = position + i;
                depth = depth + aim * i;
            },
            Movement::Up(i) => {
                aim = aim - i;
            },
            Movement::Down(i) => {
                aim = aim + i;
            },
        }
    }

    println!("FOO: position: {}; depth: {}; aim: {}", position, depth, aim);

    Ok(position * depth)
}

fn main() -> Result<(), Error> {
    let lines = stdin().lines();

    let mut parsed_lines = vec![];
    for line in lines {
        parsed_lines.push(parse_line(line?.as_str())?);
    }

    let res = get_total(parsed_lines.into_iter())?;
    println!("Answer: {}", res);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_line() {
        assert_eq!(parse_line("forward 3").unwrap(), Movement::Forward(3));
        assert_eq!(parse_line("up 2").unwrap(), Movement::Up(2));
        assert_eq!(parse_line("down 5").unwrap(), Movement::Down(5));
    }

    #[test]
    fn test_total_position_depth() {
        let lines: Vec<&str> = vec![
            "forward 5",
            "down 5",
            "forward 8",
            "up 3",
            "down 8",
            "forward 2",
        ];

        let mut parsed_lines = vec![];
        for line in lines {
            parsed_lines.push(parse_line(line).unwrap());
        }

        assert_eq!(get_total(parsed_lines.into_iter()).unwrap(), 900);
    }
}
