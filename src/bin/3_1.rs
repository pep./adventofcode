// Copyright (C) 2021 "Maxime “pep” Buquet <pep@bouah.net>"
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

use aoc2021::Error;

fn main() -> Result<(), Error> {
    let lines: Vec<String> = std::fs::read_to_string("input")
        .unwrap()
        .trim()
        .lines()
        .map(|l| String::from(l))
        .collect();

    let mask: u32 = {
        let mut tmp = 1u32;
        let len = lines[0].len();
        for _ in 1..len {
            tmp = (tmp << 1) + 1;
        }
        tmp
    };

    let values = lines
        .into_iter()
        .map(|l| u32::from_str_radix(&l, 2).unwrap())
        .collect::<Vec<u32>>();

    let res = values
        .into_iter()
        .fold(Vec::new(), |mut acc: Vec<i16>, val| {
            for i in 0..15 {
                if acc.len() == i {
                    acc.push(0);
                }

                if (val >> i) & 1 == 1 {
                    acc[i] = acc[i] + 1;
                } else {
                    acc[i] = acc[i] - 1;
                }
            }
            acc
        })
        .into_iter()
        .fold((0u8, 0u32, 0u32), |(i, mut epsilon, mut gamma), val| {
            if val > 0 {
                gamma = gamma + (1 << i);
            } else if val < 0 {
                epsilon = epsilon + (1 << i);
            }

            (i + 1, epsilon & mask, gamma)
        });

    println!("Answer: {:?}", res.1 * res.2);
    Ok(())
}
