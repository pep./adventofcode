// Copyright (C) 2021 "Maxime “pep” Buquet <pep@bouah.net>"
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

use std::fmt;
use std::error::Error as StdError;
use std::num::ParseIntError;
use std::io::Error as IOError;

#[derive(Debug)]
pub enum Error {
    ParseError,
    IOError(IOError),
    ParseIntError(ParseIntError),
}

impl StdError for Error {}

impl fmt::Display for Error {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::ParseError => write!(fmt, "Parse error"),
            Error::IOError(e) => write!(fmt, "IO error: {}", e),
            Error::ParseIntError(e) => write!(fmt, "ParseInt error: {}", e),
        }
    }
}

impl From<IOError> for Error {
    fn from(err: IOError) -> Error {
        Error::IOError(err)
    }
}

impl From<ParseIntError> for Error {
    fn from(err: ParseIntError) -> Error {
        Error::ParseIntError(err)
    }
}
